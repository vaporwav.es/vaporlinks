var express = require('express');
require('dotenv').config()
var cors = require("cors");
var bodyParser = require("body-parser")
var app = express();
const path = require("path")
const dbConnector = require("./utils/database")
const apiRouter = require("./routes/apiRouter")
const LinkService = require("./services/LinkService")

app.use(cors());
app.use(express.static(path.join(__dirname, 'static')));
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json())

app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "X-Requested-With");
    next();
});

app.use(dbConnector);

app.get('/', (req, res, next) => { res.redirect("https://soundcloud.com/wavetearz") })
app.use('/api', apiRouter)
app.get('/:link', LinkService.handleLinkRequest)

module.exports = app;
