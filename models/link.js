const mongoose = require('mongoose');

const linkSchema = mongoose.Schema({
    linkName: {type: String, required: true},
    links: {type: Array, required: true},
    title: {type: String, required: false},
    subtitle: {type: String, required: false}
});

module.exports = mongoose.model("Link", linkSchema);
