var express = require('express');
var router = express.Router();
const linkModel = require("../models/link")

router.post("/getPages", (req, res, next) => {
    if(req.body.username === process.env.MASTER_USER && req.body.password === process.env.MASTER_PASS) {
        linkModel.find({}).exec().then((result) => {
            res.send(result)
        })
    }else {
        res.send("wrong credentials")
    }
})

router.post("/addPage", (req, res, next) => {
    if(req.body.username === process.env.MASTER_USER && req.body.password === process.env.MASTER_PASS) {
        var page = new linkModel({
            "linkName": req.body.linkName,
            "links": req.body.links,
            "title": req.body.title,
            "subtitle": req.body.subtitle
        })
        page.save().then(saved => {
            res.send(saved)
        })
    }else {
        res.send("wrong credentials")
    }
})


module.exports = router;
