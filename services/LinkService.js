const linkModel = require("../models/link")
const { generateWebsite } = require("../utils/generateWebsite")

exports.handleLinkRequest = (req, res, next) => {
    linkModel.findOne({linkName: req.params.link}).exec().then(result => {
        if(result) {
            res.send(generateWebsite(result.title, result.subtitle, result.links))
        }else {
            res.redirect("https://soundcloud.com/wavetearz")
        }
    })
}