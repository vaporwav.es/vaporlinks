const serviceDetails = require("./serviceDetails.json")

exports.generateWebsite = (title, subtitle, links) => {

    var linksHTML = ""
    links.forEach((val, index) => {
        linksHTML = linksHTML +
        "<a class='button button-" + val.serviceName + "' href='" + val.link + "' target='_blank' rel='noopener'>" +
        "<img class='icon' src='images/icons/" + val.serviceName + ".svg' alt='service'>" + val.serviceButtonText +
        "</a><br>"
    })

    return "<!DOCTYPE html><html lang='en'><head><meta charset='utf-8'>" +
        "<title>" + title + "</title>" +
        "<meta name='description' content='vaporlinks'><meta name='viewport' content='width=device-width, initial-scale=1'><link href='https://fonts.googleapis.com/css?family=Open+Sans:400,600,700,800&display=swap' rel='stylesheet'><link rel='stylesheet' href='css/normalize.css'><link rel='stylesheet' href='css/skeleton-dark.css'><link rel='stylesheet' href='css/brands.css'><link rel='icon' type='image/png' href='images/avatar.png'></head>" +
        "<body><div class='container'><div class='row'><div class='column' style='margin-top: 10%'><img src='images/logo.svg' class='avatar' srcset='images/logo.svg 2x' alt='LittleLink Logo'>" +
        "<h1>" + title + "</h1>" +
        "<p>" + subtitle + "</p>" +
        linksHTML +
        "<br><br></div></div></div></body></html>"
}